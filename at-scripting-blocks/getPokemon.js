const table=base.getTable("tblST7UBFMIx45g0c");
console.log(table);
class getPokemon{
    constructor(){

    }

    async getJsonPokemon(value){
        let newURL="https://pokeapi.co/api/v2/pokemon/"+value;
        let options={
            method:"GET",
            headers:{
                "Content-Type":"application/json"
            }
        }
        const results=await fetch(newURL,options);
        let response=await results.json();
        return response;
    }

    async main(newURL){
        let jsonPokemon=await this.getJsonPokemon(newURL);
        return jsonPokemon;
    }
}

class getPokemonInfo{
    constructor(pokemonJson){
        this.pokemon=pokemonJson;
    }

    async getPokeName(){
        let pokeName=this.pokemon.name;
        return pokeName;
    }

    async getPokeHeight(){
        let pokeHeight=this.pokemon.height/10;
        return pokeHeight;
    }

    async getPokeWeight(){
        let pokeWeight=this.pokemon.weight/10;
        return pokeWeight;
    }

    async getPokeBaseExperience(){
        let pokeBaseExperience=this.pokemon.base_experience;
        return pokeBaseExperience;
    }

    async getPokeHp(){
        let pokeHp=this.pokemon.stats[0].base_stat;
        return pokeHp;
    }

    async getPokeAttack(){
        let pokeAttack=this.pokemon.stats[1].base_stat;
        return pokeAttack;
    }

    async getPokeDefense(){
        let pokeDefense=this.pokemon.stats[2].base_stat;
        return pokeDefense;
    }

    async getPokeGeneration(){
        let pokedex=this.pokemon.order;
        if(pokedex>=1 && pokedex<=151){
            return 1;
        }
        else{
            return 2;
        }
    }

    async getPokeSprites(){
        let arrayPokeSprites=[];
        let pokeSprites=Object.values(this.pokemon.sprites);
        let indexOfOthers=pokeSprites.indexOf("other");
        pokeSprites.splice(indexOfOthers,1);
        let indexOfVersions=pokeSprites.indexOf("versions");
        pokeSprites.splice(indexOfVersions,1);
        for (let i=0;i<pokeSprites.length;i++){
            if(pokeSprites[i]!=null){
                arrayPokeSprites.push(pokeSprites[i]);
            }
        }
        return arrayPokeSprites;
    }

    async getPokeGames(){
        let arrayPokeGames=[];
        let pokeGames=this.pokemon.game_indices;
        for (let i=0;i<pokeGames.length;i++){
            arrayPokeGames.push(pokeGames[i].version.name);
        }
        return arrayPokeGames;
    }

    async getPokeTypes(){
        let arrayPokeTypes=[];
        let pokeTypes=this.pokemon.types;
        for(let i=0;i<pokeTypes.length;i++){
            arrayPokeTypes.push(pokeTypes[i].type.name);
        }
        return arrayPokeTypes;
    }

    async getPokeOrder(){
        let pokeOrder=this.pokemon.order;
        return pokeOrder;
    }

    async getPokeAbilities(){
        let arrayAbilities=[];
        let abilities=this.pokemon.abilities;
        for(let i=0;i<abilities.length;i++){
            arrayAbilities.push(abilities[i].ability.name);
        }
        return arrayAbilities;
    }
}

class createRecord{
    constructor(poke){
        this.pokeID=poke;
    }

    async buildAttachmentField(attachments){
        let arrayAttachment=[];
        for(let i=0;i<attachments.length;i++){
            let jsonAttachment={"url":attachments[i]};
            arrayAttachment.push(jsonAttachment);
        }
        return arrayAttachment;
    }

    async addRecord(records){
        try{let url="https://api.airtable.com/v0/appUf2dgW9VhVWDfI/tblST7UBFMIx45g0c";
        let input_data =  {
            "records" : records,
            "typecast": true
        };
        let getOptions = {
            method: "post",
            headers: {
                "Authorization" : "Bearer keyh9ej9IuVtqlsaU",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(input_data)
        };
        var getResults = await fetch(url, getOptions);
        let response = await getResults.json();
        console.log(response);}
        catch(e){
            console.log(e);
        }
    }

    async main(){
        let pokemon=new getPokemon();
        let jsonPokemon=await pokemon.main(this.pokeID);
        //console.log(jsonPokemon);
        let pokemonInfo=new getPokemonInfo(jsonPokemon);
        let newElement={};
        newElement.fldpMq2Si0xW2lJy0=await pokemonInfo.getPokeName();
        newElement.fldkAKruubC69n4gX=await this.buildAttachmentField(await pokemonInfo.getPokeSprites());
        newElement.fld8Yy6rbBAZOMpyB=await pokemonInfo.getPokeHeight();
        newElement.fldsi4gbEXTZBlXVT=await pokemonInfo.getPokeWeight();
        newElement.fldb9c9lwF13iBrT6=await pokemonInfo.getPokeBaseExperience();
        newElement.fldjR8nFn9uxhRUqG=await pokemonInfo.getPokeHp();
        newElement.fldEeXPIQtM6hQ9h4=await pokemonInfo.getPokeAttack();
        newElement.fldqEtU1Wwo534ClN=await pokemonInfo.getPokeDefense();
        newElement.fld6gbXLKpGGElpO1=await pokemonInfo.getPokeGames();
        newElement.fldApBuHPFtSz5yZP=await pokemonInfo.getPokeTypes();
        newElement.fldvfjjQxXelWv5Fj=await pokemonInfo.getPokeAbilities();
        newElement.fldxsJW8txgJWgg6T=await pokemonInfo.getPokeOrder();
        newElement.fldMqQocTwSavd4NI=await pokemonInfo.getPokeGeneration();
        let arrayElement=[{fields:newElement}];
        await this.addRecord(arrayElement)
    }
}

let record = await input.recordAsync('Select a record to use', table);
let value=record.getCellValue("fldpMq2Si0xW2lJy0").toLowerCase().replace(". ","-");
let addPokemon=new createRecord(value);
await addPokemon.main();
